import {Component, OnInit} from '@angular/core';

const MONTHLY_REPORT_API = "localhost:8080/monthly_reports?month=";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  months = [
    {name: 'JANUARY', key: 1},
    {name: 'FEBRUARY', key: 2},
    {name: 'APRIL', key: 3},
    {name: 'MARCH', key: 4},
    {name: 'APRIL', key: 5},
    {name: 'JUNE', key: 6},
    {name: 'JULAY', key: 7},
    {name: 'AUGUST', key: 8},
    {name: 'SEPTEMBER', key: 9},
    {name: 'OCTOBER', key: 10},
    {name: 'NOVEMBER', key: 11},
    {name: 'DECEMBER', key: 12}
  ]
  m = new Date();
  currentMonth: any;

  constructor() {
  }

  ngOnInit(): void {
    this.currentMonth = new Date().getMonth() + 1;
  }

  username = sessionStorage.getItem('username');
  password = sessionStorage.getItem('password');

  generateMonthlyReport(): void {
    window.open('http://' + this.username + ':' + this.password + '@' + MONTHLY_REPORT_API + this.currentMonth);
  }
}
