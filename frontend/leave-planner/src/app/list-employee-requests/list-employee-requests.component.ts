import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

const EMPLOYEES_REQUESTS_API = "http://localhost:8080/requests";

@Component({
  selector: 'app-list-employee-requests',
  templateUrl: './list-employee-requests.component.html',
  styleUrls: ['./list-employee-requests.component.css']
})
export class ListEmployeeRequestsComponent implements OnInit {

  employeesRequests: any;
  p: number = 1;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.http.get(EMPLOYEES_REQUESTS_API).subscribe({
      next: (data) => {
        console.log(data)
        this.employeesRequests = data;
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  getManagerReplyById(id: number) {
    this.router.navigate(['/request/manager_reply/' + id]);
  }

}
