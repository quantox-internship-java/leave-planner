import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEmployeeRequestsComponent } from './list-employee-requests.component';

describe('ListEmployeeRequestsComponent', () => {
  let component: ListEmployeeRequestsComponent;
  let fixture: ComponentFixture<ListEmployeeRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListEmployeeRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEmployeeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
