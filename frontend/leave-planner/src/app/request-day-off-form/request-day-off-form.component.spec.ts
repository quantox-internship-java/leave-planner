import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestDayOffFormComponent } from './request-day-off-form.component';

describe('RequestDayOffFormComponent', () => {
  let component: RequestDayOffFormComponent;
  let fixture: ComponentFixture<RequestDayOffFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestDayOffFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestDayOffFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
