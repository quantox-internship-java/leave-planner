import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";

const REQUEST_CREATE_API = "http://localhost:8080/request/create";

@Component({
  selector: 'app-request-day-off-form',
  templateUrl: './request-day-off-form.component.html',
  styleUrls: ['./request-day-off-form.component.css']
})
export class RequestDayOffFormComponent implements OnInit {

  endDateBeforeStart: boolean = false;
  isStartDateNull: boolean = true;
  isEndDateNull: boolean = true;

  form: any = {
    type: null,
    startDate: null,
    endDate: null,
    description: null,
    userId: Number(sessionStorage.getItem('userId'))
  }

  constructor(private router: Router, private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  createRequest(): void {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      responseType: 'text'
    };

    this.http.post(REQUEST_CREATE_API, this.form, httpOptions).subscribe({
      next: () => {
        this.router.navigate(['/home']);
      },
      error: (err) => {
        console.log(err);
      }
    })
  }

  invalidDate(): boolean {
    if (this.form.startDate != null && this.form.endDate != null) {
      this.isStartDateNull = false;
      this.isEndDateNull = false;
      if (this.form.endDate < this.form.startDate) {
        this.endDateBeforeStart = true;
        return true;
      } else {
        this.endDateBeforeStart = false;
        return false;
      }
    } else {
      this.isStartDateNull = true;
      this.isEndDateNull = true;
      return true;
    }
  }

}
