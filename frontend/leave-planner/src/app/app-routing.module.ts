import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from "./home/home.component";
import {EmployeesComponent} from "./employees/employees.component";
import {UsersComponent} from "./users/users.component";
import {RolesComponent} from "./roles/roles.component";
import {EmployeesFormComponent} from "./employees/employees-form/employees-form.component";
import {UsersFormComponent} from "./users/users-form/users-form.component";
import {RequestDayOffFormComponent} from "./request-day-off-form/request-day-off-form.component";
import {ListEmployeeRequestsComponent} from "./list-employee-requests/list-employee-requests.component";
import {ManagerReplyComponent} from "./manager-reply/manager-reply.component";
import {LogoutComponent} from "./logout/logout.component";
import {AuthGuard} from "./authentication/auth.guard";
import {ReportsComponent} from "./reports/reports.component";

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'employees', component: EmployeesComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
  {path: 'roles', component: RolesComponent, canActivate: [AuthGuard]},
  {path: 'employee/create', component: EmployeesFormComponent, canActivate: [AuthGuard]},
  {path: 'employee/update/:id', component: EmployeesFormComponent, canActivate: [AuthGuard]},
  {path: 'employee/update', component: EmployeesFormComponent, canActivate: [AuthGuard]},
  {path: 'users/create', component: UsersFormComponent, canActivate: [AuthGuard]},
  {path: 'users/update/:id', component: UsersFormComponent, canActivate: [AuthGuard]},
  {path: 'request/create', component: RequestDayOffFormComponent, canActivate: [AuthGuard]},
  {path: 'users/update/:id', component: UsersFormComponent, canActivate: [AuthGuard]},
  {path: 'requests', component: ListEmployeeRequestsComponent, canActivate: [AuthGuard]},
  {path: 'request/manager_reply/:id', component: ManagerReplyComponent, canActivate: [AuthGuard]},
  {path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]},
  {path: 'request/listAllRequests:id', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'reports', component: ReportsComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
