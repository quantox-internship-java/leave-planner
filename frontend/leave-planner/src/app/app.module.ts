import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HomeComponent} from './home/home.component';
import {EmployeesComponent} from './employees/employees.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {UsersComponent} from './users/users.component';
import {RolesComponent} from './roles/roles.component';
import {EmployeesFormComponent} from './employees/employees-form/employees-form.component';
import {UsersFormComponent} from './users/users-form/users-form.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from "@angular/material/select";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RequestDayOffFormComponent} from './request-day-off-form/request-day-off-form.component';
import {ListEmployeeRequestsComponent} from './list-employee-requests/list-employee-requests.component';
import {NgChartsModule} from 'ng2-charts';
import {ManagerReplyComponent} from './manager-reply/manager-reply.component';
import {BasicAuthHttpInterceptor} from "./authentication/basic-auth-interceptor";
import {LogoutComponent} from './logout/logout.component';
import {AuthGuard} from "./authentication/auth.guard";
import {AuthService} from "./authentication/auth.service";
import {MenuComponent} from './menu/menu.component';
import {ReportsComponent} from './reports/reports.component';
import {NgxMatSelectSearchModule} from "ngx-mat-select-search";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatInputModule} from "@angular/material/input";
import {Ng2SearchPipeModule} from "ng2-search-filter";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    EmployeesComponent,
    UsersComponent,
    RolesComponent,
    EmployeesFormComponent,
    UsersFormComponent,
    RequestDayOffFormComponent,
    UsersFormComponent,
    ListEmployeeRequestsComponent,
    ManagerReplyComponent,
    LogoutComponent,
    MenuComponent,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    BrowserAnimationsModule,
    NgChartsModule,
    NgxMatSelectSearchModule,
    MatAutocompleteModule,
    MatInputModule,
    Ng2SearchPipeModule
  ],
  exports: [
    MatFormFieldModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: BasicAuthHttpInterceptor, multi: true},
    AuthGuard,
    AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
