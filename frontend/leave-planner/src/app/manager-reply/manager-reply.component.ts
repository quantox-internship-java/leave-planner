import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

const REQUEST_GET_API = "http://localhost:8080/request/"
const MANAGER_REPLY_API = "http://localhost:8080/request/manager_reply";

@Component({
  selector: 'app-manager-reply',
  templateUrl: './manager-reply.component.html',
  styleUrls: ['./manager-reply.component.css']
})
export class ManagerReplyComponent implements OnInit {

  id: number;
  private routeSub: Subscription;

  form: any = {
    name: null,
    type: null,
    startDate: null,
    endDate: null,
    description: null,
    employee: {
      name: null
    },
    id: null,
    requestStatus: null,
    requestDescription: null,
    requestDate: null,
  }

  managerReplyObject: any = {
    id: null,
    requestStatus: null,
    requestDescription: null,
    requestDate: null,
  }

  constructor(private router: Router, private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id']
    });
    if (this.id != undefined) {
      this.http.get(REQUEST_GET_API + this.id).subscribe({
        next: (data) => {
          this.form = data;
        },
        error: (err) => {
          console.log(err)
        }
      })
    }
  }

  managerReply(): void {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      responseType: 'text'
    };

    this.managerReplyObject.requestStatus = this.form.requestStatus;
    this.managerReplyObject.id = this.form.id;
    this.managerReplyObject.requestDescription = this.form.requestDescription;
    this.managerReplyObject.requestManagerDto = this.form.requestManagerDto;

    this.http.post(MANAGER_REPLY_API, this.managerReplyObject, httpOptions).subscribe({
      next: () => {
        this.router.navigate(['/home']);
      }
    })
  }

}
