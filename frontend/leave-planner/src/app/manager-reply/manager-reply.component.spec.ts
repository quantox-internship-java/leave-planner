import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ManagerReplyComponent} from './manager-reply.component';

describe('ManagerReplyComponent', () => {
  let component: ManagerReplyComponent;
  let fixture: ComponentFixture<ManagerReplyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ManagerReplyComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
