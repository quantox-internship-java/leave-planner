import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

const USERS_API = "http://localhost:8080/users";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any;
  p: number = 1;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.http.get(USERS_API).subscribe({
      next: (data) => {
        this.users = data;
      },
      error: (err) => {
        console.log(err);
      }
    })
  }

  getUserById(id: number){
    this.router.navigate(['/users/update/' + id]);
  }
}
