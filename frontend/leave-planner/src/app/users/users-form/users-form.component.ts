import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

const USER_GET_API = 'http://localhost:8080/users/';
const USER_CREATE_API = 'http://localhost:8080/users/create';
const USER_EDIT_API = 'http://localhost:8080/users/update/';
const EMPLOYEES_API = 'http://localhost:8080/employees';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  id: number;
  employeesList: any;
  uniqueUsername: boolean = false;
  isDisabled: boolean = false;
  private routeSub: Subscription;

  form: any = {
    username: null,
    email: null,
    password: null,
    status: null,
    role: {
      id: null,
      name: null,
      description: null
    },
    employees: null
  }

  constructor(private router: Router, private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    if (this.id != undefined) {
      this.http.get(USER_GET_API + this.id).subscribe({
        next: (data) => {
          this.form = data;
          this.form.password = null;
        },
        error: (err) => {
          console.log(err);
        }
      })
    }
    this.listEmployees();
  }

  saveUser(): void {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      responseType: 'text'
    };

    this.http.post(USER_CREATE_API, this.form, httpOptions).subscribe({
      next: () => {
        this.router.navigate(['/users']);
      },
      error: (err) => {
        if (err.status == 406) {
          this.uniqueUsername = true;
        }
      }
    })
  }

  editUser(id: number): void {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      responseType: 'text'
    };

    this.http.post(USER_EDIT_API + id, this.form, httpOptions).subscribe({
      next: () => {
        this.router.navigate(['/users'])
      },
      error: (err) => {
        if (err.status == 406) {
          this.uniqueUsername = true;
        }
      }
    })
  }

  listEmployees(): void {
    this.http.get(EMPLOYEES_API).subscribe({
      next: (data) => {
        this.employeesList = data;
      },
      error: (err) => {
        console.log(err)
      }
    })
  }
}
