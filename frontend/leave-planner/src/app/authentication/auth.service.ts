import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs";

const AUTH_API = 'http://localhost:8080/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn: boolean;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  authenticate(username: string, password: string) {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': "Basic " + btoa(username + ":" + password)
      }),
      responseType: 'text'
    };

    return this.http.post(AUTH_API, {}, httpOptions).pipe(
      map(
        (data) => {
          let authString = 'Basic ' + btoa(username + ':' + password);
          sessionStorage.setItem('username', username);
          sessionStorage.setItem('basicAuth', authString);
          sessionStorage.setItem('role', JSON.parse(data.toString()).role.name);
          sessionStorage.setItem('userId', JSON.parse(data.toString()).id);
          if (JSON.parse(data.toString()).employee != null) {
            sessionStorage.setItem('userEmployeeId', JSON.parse(data.toString()).employee.id);
          } else {
            sessionStorage.setItem('userEmployeeId', String(-1));
          }
          this.isLoggedIn = true;
        }
      ))
  }

  isUserLoggedIn() {
    let basicAuth = sessionStorage.getItem('basicAuth');
    return basicAuth != null;
  }

  logOut() {
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('basicAuth');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('userId');
    sessionStorage.removeItem('userEmployeeId');

    this.isLoggedIn = false;
  }
}
