import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

const EMPLOYEES_API = "http://localhost:8080/employees";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: any;
  p: number = 1;
  userRole = sessionStorage.getItem('role');

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
    this.http.get(EMPLOYEES_API).subscribe({
      next: (data) => {
        this.employees = data;
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  getEmployeeById(id: number) {
    this.router.navigate(['/employee/update/' + id]);
  }
}
