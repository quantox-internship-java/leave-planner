import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

const EMPLOYEE_GET_API = 'http://localhost:8080/employee/';
const EMPLOYEE_CREATE_API = 'http://localhost:8080/employee/create';
const EMPLOYEE_EDIT_API = 'http://localhost:8080/employee/update/';
const USERS_GET_API = 'http://localhost:8080/users';

@Component({
  selector: 'app-employees-form',
  templateUrl: './employees-form.component.html',
  styleUrls: ['./employees-form.component.css']
})
export class EmployeesFormComponent implements OnInit {

  id: number;
  uniqueEmbg: boolean = false;
  private routeSub: Subscription;

  searchText: any;

  form: any = {
    name: null,
    surname: null,
    embg: null,
    email: null,
    status: null,
    projectName: null,
    type: null,
    country: null,
    office: null,
    user: {
      id: null
    }
  }

  userForm: any = [
    {
      id: null,
      name: null
    }
  ]

  constructor(private router: Router, private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id']
    });
    if (this.id != undefined) {
      this.http.get(EMPLOYEE_GET_API + this.id).subscribe({
        next: (data) => {
          this.form = data;
          console.log(this.form)
        },
        error: (err) => {
          console.log(err)
        }
      })
    }

    this.http.get(USERS_GET_API).subscribe({
      next: (data) => {
        this.userForm = data;
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  saveEmployee(): void {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      responseType: 'text'
    };

    this.http.post(EMPLOYEE_CREATE_API, this.form, httpOptions).subscribe({
      next: () => {
        this.router.navigate(['/employees']);
      },
      error: (err) => {
        if (err.status == 406) {
          this.uniqueEmbg = true;
        }
      }
    })
  }

  editEmployee(id: number): void {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      responseType: 'text'
    };

    this.http.post(EMPLOYEE_EDIT_API + id, this.form, httpOptions).subscribe({
      next: () => {
        this.router.navigate(['/employees']);
      },
      error: (err) => {
        if (err.status == 406) {
          this.uniqueEmbg = true;
        }
      }
    })
  }
}
