import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

const ROLES_API = "http://localhost:8080/roles";

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  roles: any;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get(ROLES_API).subscribe({
      next: (data) => {
        this.roles = data;
      },
      error: (err) => {
        console.log(err)
      }
    })
  }
}
