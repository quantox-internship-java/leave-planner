import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../authentication/auth.service";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private logoutService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.logoutService.logOut();
    this.router.navigate(['login']);
  }

}
