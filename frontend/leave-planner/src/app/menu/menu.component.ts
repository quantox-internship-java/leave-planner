import {Component, OnInit} from '@angular/core';
import {AuthService} from "../authentication/auth.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  userRole: any;

  constructor(public authenticationService: AuthService) {
  }

  ngOnInit(): void {
    this.userRole = sessionStorage.getItem('role');
  }
}
