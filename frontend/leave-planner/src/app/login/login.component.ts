import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../authentication/auth.service";

const HTTP_UNAUTHORIZED: number = 401;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {
    username: null,
    password: null
  };

  isLoggedIn = false;
  errorMessage = '';
  roles: string[] = [];
  wrongCredentials: boolean;
  invalidLogin: boolean = false;

  constructor(private router: Router,
              private loginService: AuthService) {
  }

  ngOnInit(): void {
    // TODO document why this method 'ngOnInit' is empty
  }

  onSubmit(): void {
    sessionStorage.setItem('username', this.form.username);
    sessionStorage.setItem('password', this.form.password);

    this.loginService.authenticate(this.form.username, this.form.password).subscribe(
      () => {
        this.router.navigate(['/home']);
        this.invalidLogin = false;
      },
      (err) => {
        this.invalidLogin = true;
        if (err.status == HTTP_UNAUTHORIZED) {
          this.wrongCredentials = true;
        }
      }
    )
  }
}
