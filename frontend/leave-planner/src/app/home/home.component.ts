import {Component, OnInit} from '@angular/core';
import {ChartData, ChartOptions} from "chart.js";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

const REQUESTS_PER_TYPE_API = "http://localhost:8080/request/groupByType";
const REMAINING_DAYS_OFF_API = "http://localhost:8080/request/days_off_remaining";
const LIST_ALL_REQUESTS_API = "http://localhost:8080/request/listAllRequests/";
const GREEN = "#598253";
const BLUE = "#6c83a1"

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  username: string | null;
  requests: any;

  form: any;

  p: number = 1;

  dayOff: number = 0;
  sickLeave: number = 0;
  vacation: number = 0;
  numRequests: number[] = [];
  requestsTypeData: any;
  requestChartLoaded = false;

  daysOff: number[] = [];
  daysOffRemainingData: any;
  daysOffChartLoaded = false;

  requestsPieChartData: ChartData<"pie"> = {
    labels: [],
    datasets: []
  };

  requestsPieChartOptions: ChartOptions<"pie"> = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      tooltip: {
        callbacks: {
          label: function (context) {
            let label = context.dataset.label || '';

            if (label) {
              label += ': ';
            }
            if (context.parsed !== null) {
              label += new Intl.NumberFormat().format(context.parsed);
            }
            return label;
          }
        }
      }
    }
  };

  dayOffPieChartData: ChartData<"doughnut"> = {
    labels: [],
    datasets: []
  };

  dayOffPieChartOptions: ChartOptions<"doughnut"> = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      tooltip: {
        callbacks: {
          label: function (context) {
            let label = context.dataset.label || '';

            if (label) {
              label += ': ';
            }
            if (context.parsed !== null) {
              label += new Intl.NumberFormat().format(context.parsed);
            }
            return label;
          }
        }
      }
    }
  }

  constructor(private router: Router, private http: HttpClient) {
  }

  ngOnInit(): void {
    this.username = sessionStorage.getItem('username');

    this.http.get(REQUESTS_PER_TYPE_API).subscribe({
      next: (data) => {
        this.requestsTypeData = data;
        for (const s of this.requestsTypeData) {
          let userId = s.split(",")[0];
          if (sessionStorage.getItem('userEmployeeId') != String(-1)) {
            if (userId == sessionStorage.getItem('userEmployeeId')) {
              let numRequestsPerType = s.split(",")[2];
              if (s.includes("Day off")) {
                this.dayOff = Number(numRequestsPerType);
                this.numRequests[0] = this.dayOff;
              } else if (s.includes("Sick leave")) {
                this.sickLeave = Number(numRequestsPerType);
                this.numRequests[1] = this.sickLeave;
              } else {
                this.vacation = Number(numRequestsPerType);
                this.numRequests[2] = this.vacation;
              }
            }
          }
        }

        for (let i = 0; i < 3; i++) {
          if (this.numRequests[i] == undefined) {
            this.numRequests[i] = 0;
          }
        }

        this.requestsPieChartData.labels?.push(
          "Days off: " + this.dayOff, "Sick Leave: " + this.sickLeave, "Vacation: " + this.vacation
        )

        this.requestsPieChartData.datasets.push(
          {data: this.numRequests}
        );

        this.requestChartLoaded = true;
      },
      error: (err) => {
        console.log(err)
      }
    })
    this.listAllRequests();

    this.http.get(REMAINING_DAYS_OFF_API).subscribe({
      next: (data) => {
        this.daysOffRemainingData = data;
        for (const s of this.daysOffRemainingData) {
          let userId = s.split(",")[0];
          if (sessionStorage.getItem('userEmployeeId') != String(-1)) {
            if (userId == sessionStorage.getItem('userEmployeeId')) {
              let totalDaysOff = s.split(",")[1];
              this.daysOff[0] = Number(totalDaysOff);

              let remaining = s.split(",")[2];
              this.daysOff[1] = Number(remaining);
            }
          }
        }

        if (this.daysOff[0] == undefined) {
          this.daysOff[0] = 0;
          this.daysOff[1] = 20;
        }

        this.dayOffPieChartData.labels?.push(
          "Days off: " + this.daysOff[0], "Remaining: " + this.daysOff[1]
        )

        this.dayOffPieChartData.datasets.push({
          data: this.daysOff,
          "backgroundColor": [GREEN, BLUE],
          "hoverBackgroundColor": [GREEN, BLUE],
          "hoverBorderColor": [GREEN, BLUE],
        })

        this.daysOffChartLoaded = true;
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  listAllRequests() {
    this.http.get(LIST_ALL_REQUESTS_API + sessionStorage.getItem("userId")).subscribe({
      next: (data) => {
        this.form = data;
      },
      error: (err) => {
        console.log(err)
      }
    })
  }
}
