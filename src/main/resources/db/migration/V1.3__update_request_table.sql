alter table request
    add request_status varchar(255),
    add request_description varchar(255),
    add request_date datetime,
    add request_manager int,
    add (foreign key (request_manager) references user(id));
