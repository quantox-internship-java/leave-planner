create table if not exists request(
    id int not null auto_increment primary key,
    start_date datetime not null,
    end_date datetime not null,
    type varchar(255) not null,
    description varchar(255) not null,
    employee int not null,
    foreign key (employee) references employee(id)
);