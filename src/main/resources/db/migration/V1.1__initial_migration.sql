create table if not exists role
(
    id          int          not null auto_increment primary key,
    name        varchar(255) not null,
    description varchar(255) not null
);

create table if not exists user
(
    id       int          not null auto_increment primary key,
    username varchar(255) not null,
    email    varchar(255) not null,
    password varchar(255) not null,
    role     int          not null,
    status   tinyint,
    foreign key (role) references role (id)
);

create table if not exists employee
(
    id            int          not null auto_increment primary key,
    name          varchar(255) not null,
    surname       varchar(255) not null,
    embg          varchar(255) not null unique,
    email         varchar(255) not null,
    status        varchar(255) not null,
    employee_type varchar(255) not null,
    country       varchar(255) not null,
    office        varchar(255) not null,
    user_employee int          null,
    foreign key (user_employee) references user (id)
);

create table employee_users
(
    employee_id int not null,
    users_id    int not null,
    primary key (employee_id, users_id),
    foreign key (employee_id) references employee (id),
    foreign key (users_id) references user (id)
);
