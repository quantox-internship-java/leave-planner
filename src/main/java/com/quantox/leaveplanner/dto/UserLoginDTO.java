package com.quantox.leaveplanner.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * This class transfers the login info for the User
 */
@Data
public class UserLoginDTO {

    /**
     * This fields holds the username of the User
     */
    @NotEmpty
    private String username;

    /**
     * This field holds the password of the User
     */
    @NotEmpty
    private String password;
}
