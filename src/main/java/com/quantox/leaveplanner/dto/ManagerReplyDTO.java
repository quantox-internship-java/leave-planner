package com.quantox.leaveplanner.dto;

import com.quantox.leaveplanner.model.enumerations.RequestStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * DTO class for Manager Reply
 */
@Getter
@Setter
public class ManagerReplyDTO {

    /**
     * The id of the manager that replies to the request
     */
    private Long id;

    /**
     * Request's status (PENDING, APPROVED, DECLINED)
     */
    private RequestStatus requestStatus;

    /**
     * Description of the response from the manager to the request
     */
    private String requestDescription;

    /**
     * The date of the response from the manager
     */
    private LocalDate requestDate;

    /**
     * The manager that approves the request
     */
    private UserDTO requestManagerDto;
}
