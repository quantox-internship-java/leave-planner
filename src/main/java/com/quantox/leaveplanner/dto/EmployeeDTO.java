package com.quantox.leaveplanner.dto;

import com.quantox.leaveplanner.model.User;
import com.quantox.leaveplanner.model.enumerations.Status;
import com.quantox.leaveplanner.model.enumerations.Type;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * DTO class for creating and updating employees
 */
@Getter
@Setter
public class EmployeeDTO {

    /**
     * Employee's id
     */
    private Long id;

    /**
     * Employee's name
     */
    @NotEmpty
    private String name;

    /**
     * Employee's surname
     */
    @NotEmpty
    private String surname;

    /**
     * Employee's unique master citizen number
     */
    @NotEmpty
    private String embg;

    /**
     * Employee's email
     */
    @NotEmpty
    @Email
    private String email;

    /**
     * Employee's status
     */
    @NotNull
    private Status status;

    /**
     * Employee's project
     */
    private String projectName;

    /**
     * Employee's type
     */
    @NotNull
    private Type type;

    /**
     * Employee's country
     */
    @NotEmpty
    private String country;

    /**
     * Employee's office
     */
    @NotEmpty
    private String office;

    /**
     * Employee's related user
     */
    @NotNull
    private User user;
}
