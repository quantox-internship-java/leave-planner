package com.quantox.leaveplanner.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * DTO class for creating and updating requests
 */
@Getter
@Setter
public class RequestDTO {

    /**
     * Request's start date
     */
    @NotNull
    private LocalDate startDate;

    /**
     * Request's end date
     */
    @NotNull
    private LocalDate endDate;

    /**
     * Request's type
     */
    @NotEmpty
    private String type;

    /**
     * Request's description
     */
    private String description;

    /**
     * Employee that requests the Request
     */
    @NotNull
    private Long userId;
}
