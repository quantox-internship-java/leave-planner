package com.quantox.leaveplanner.dto;

import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.Role;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * DTO class for creating and updating User
 */
@Getter
@Setter
public class UserDTO {

    /**
     * Id of the User
     */
    private Long id;

    /**
     * Username of the User
     */
    @NotEmpty
    private String username;

    /**
     * User's email
     */
    @NotEmpty
    @Email
    private String email;

    /**
     * User's password
     */
    private String password;

    /**
     * User's status
     */
    @NotNull
    private Boolean status;

    /**
     * User's role
     */
    @NotNull
    private Role role;

    /**
     * User - Employee mapping
     */
    private Employee employee;

    /**
     * Employees that the User manages
     */
    private List<Employee> employees;
}
