package com.quantox.leaveplanner.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * This class creates the entity for the User and generates boilerplate code
 */
@Getter
@Setter
@Entity
@Table(name = "user")
public class User {

    /**
     * This field holds the ID of the User
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    /**
     * This field holds the username of the User
     */
    @Column(name = "username")
    private String username;

    /**
     * This field holds the email of the User
     */
    @Column(name = "email")
    private String email;

    /**
     * This field holds the password of the User
     */
    @Column(name = "password")
    private String password;

    /**
     * This field holds the status of the User
     */
    @Column(name = "status")
    private Boolean status;

    /**
     * User's role
     */
    @ManyToOne
    @JoinColumn(name = "role")
    private Role role;

    /**
     * Employees that User manages
     */
    @JsonIgnore
    @ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
    private List<Employee> employees;

    /**
     * User - Employee mapping
     */
    @JsonIgnore
    @OneToOne(mappedBy = "user")
    private Employee employee;
}
