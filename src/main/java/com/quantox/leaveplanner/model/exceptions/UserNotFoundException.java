package com.quantox.leaveplanner.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is being thrown if the User does not exist in the database
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {
    /**
     * This method informs which User with the particular id does not exist in database
     *
     * @param id ID
     */
    public UserNotFoundException(Long id) {
        super(String.format("User with id %d not found", id));
    }
}
