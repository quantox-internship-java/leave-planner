package com.quantox.leaveplanner.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class EmployeeWithEmbgAlreadyExists extends RuntimeException {

    public EmployeeWithEmbgAlreadyExists(String embg) {
        super(String.format("Employee with embg %s already exists", embg));
    }
}
