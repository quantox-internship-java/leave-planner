package com.quantox.leaveplanner.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is being thrown if the Employee does not exist in the database
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EmployeeNotFoundException extends RuntimeException {
    /**
     * This method informs which Employee with the particular id does not exist in database
     *
     * @param id ID
     */
    public EmployeeNotFoundException(Long id) {
        super(String.format("Employee with id %d not found", id));
    }
}
