package com.quantox.leaveplanner.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quantox.leaveplanner.model.enumerations.Status;
import com.quantox.leaveplanner.model.enumerations.Type;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Entity class for Employee
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {

    /**
     * Employee's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    /**
     * Employee's name
     */
    @Column(name = "name")
    private String name;

    /**
     * Employee's surname
     */
    @Column(name = "surname")
    private String surname;

    /**
     * Employee's unique master citizen number
     */
    @Column(name = "embg")
    private String embg;

    /**
     * Employee's email
     */
    @Column(name = "email")
    private String email;

    /**
     * Employee's status (active or inactive)
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    /**
     * The project that the employee works on
     */
    @Column(name = "project_name")
    private String projectName;

    /**
     * Employee's type (permanent, probationary or intern)
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "employee_type")
    private Type type;

    /**
     * Employee's country
     */
    @Column(name = "country")
    private String country;

    /**
     * Employee's office
     */
    @Column(name = "office")
    private String office;

    /**
     * Employee's managers
     */
    @ManyToMany
    @JoinTable(
            name = "employee_users",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "users_id"))
    private List<User> users;

    /**
     * User's employee details
     */
    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "user_employee")
    private User user;

    /**
     * Request that requested the employee
     */
    @OneToMany(mappedBy = "employee")
    @JsonIgnore
    private List<Request> requests;
}
