package com.quantox.leaveplanner.model.enumerations;

/**
 * Types of employees
 */
public enum Type {
    PERMANENT,
    PROBATIONARY,
    INTERN
}