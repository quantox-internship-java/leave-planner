package com.quantox.leaveplanner.model.enumerations;

/**
 * Employees' statuses
 */
public enum Status {
    ACTIVE,
    INACTIVE
}