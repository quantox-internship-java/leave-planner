package com.quantox.leaveplanner.model.enumerations;

/**
 * Status of the Employee request
 */
public enum RequestStatus {
    PENDING,
    APPROVED,
    DECLINED
}
