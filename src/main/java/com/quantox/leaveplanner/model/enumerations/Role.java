package com.quantox.leaveplanner.model.enumerations;

public enum Role {

    ADMIN,
    EMPLOYEE,
    OFFICE_ASSISTANT,
    PROJECT_MANAGER;
}
