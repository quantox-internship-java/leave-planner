package com.quantox.leaveplanner.model;

import com.quantox.leaveplanner.model.enumerations.RequestStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Entity class for Requests
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "request")
public class Request {

    /**
     * Request's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    /**
     * Request's start date
     */
    @Column(name = "start_date")
    private LocalDate startDate;

    /**
     * Request's end date
     */
    @Column(name = "end_date")
    private LocalDate endDate;

    /**
     * Request's type
     */
    @Column(name = "type")
    private String type;

    /**
     * Request's description
     */
    @Column(name = "description")
    private String description;

    /**
     * Request status (PENDING, APPROVED or DECLINED)
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "request_status")
    private RequestStatus requestStatus;

    /**
     * Description that manager gives after confirmed/declined request
     */
    @Column(name = "request_description")
    private String requestDescription;

    /**
     * The date of the response from the manager to the request
     */
    @Column(name = "request_date")
    private LocalDate requestDate;

    /**
     * Id of the employee that requested the request
     */
    @ManyToOne
    @JoinColumn(name = "employee")
    private Employee employee;

    /**
     * Id of the manager that responded to the request
     */
    @OneToOne
    @JoinColumn(name = "request_manager")
    private User requestManager;
}
