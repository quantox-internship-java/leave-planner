package com.quantox.leaveplanner.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;

/**
 * Role class represents Leave Planner user's roles
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "role")
public class Role {

    /**
     * Role's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    /**
     * Role's name
     */
    @Column(name = "name")
    private String name;

    /**
     * Role's description
     */
    @Column(name = "description")
    private String description;

    /**
     * More than one user can have the same role
     */
    @JsonIgnore
    @OneToMany(mappedBy = "role")
    private List<User> users;
}
