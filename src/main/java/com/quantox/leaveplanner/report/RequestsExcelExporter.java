package com.quantox.leaveplanner.report;

import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.model.enumerations.RequestStatus;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * RequestsExcelExporter class is responsible for generating excel workbook
 * and populating the sheet with information about the approved requests
 */
public class RequestsExcelExporter {

    private final XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private final List<Request> requests;

    public RequestsExcelExporter(List<Request> requests) {
        this.requests = requests;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Monthly Requests");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(12);
        style.setFont(font);

        createCell(row, 0, "Request ID", style);
        createCell(row, 1, "Start date", style);
        createCell(row, 2, "End date", style);
        createCell(row, 3, "Type", style);
        createCell(row, 4, "Description", style);
        createCell(row, 5, "Employee ID", style);
        createCell(row, 6, "Request status", style);
        createCell(row, 7, "Request description", style);
        createCell(row, 8, "Request date", style);
        createCell(row, 9, "Request manager", style);
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
            cell.setCellStyle(style);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
            cell.setCellStyle(style);
        } else if (value instanceof Long) {
            cell.setCellValue((Long) value);
            cell.setCellStyle(style);
        } else if (value instanceof LocalDate) {
            XSSFCreationHelper createHelper = workbook.getCreationHelper();
            XSSFCellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setDataFormat(
                    createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
            cell.setCellStyle(cellStyle);
            cell.setCellValue((LocalDate) value);
        } else if (value instanceof RequestStatus) {
            cell.setCellValue(String.valueOf(value));
            cell.setCellStyle(style);
        } else {
            cell.setCellValue((String) value);
            cell.setCellStyle(style);
        }
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(12);
        style.setFont(font);

        for (Request request : requests) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, request.getId(), style);
            createCell(row, columnCount++, request.getStartDate(), style);
            createCell(row, columnCount++, request.getEndDate(), style);
            createCell(row, columnCount++, request.getType(), style);
            createCell(row, columnCount++, request.getDescription(), style);
            createCell(row, columnCount++, request.getEmployee().getId(), style);
            createCell(row, columnCount++, request.getRequestStatus().name(), style);
            createCell(row, columnCount++, request.getRequestDescription(), style);
            createCell(row, columnCount++, request.getRequestDate(), style);
            createCell(row, columnCount, request.getRequestManager().getId(), style);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();
    }
}