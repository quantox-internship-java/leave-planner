package com.quantox.leaveplanner.repository;

import com.quantox.leaveplanner.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * RoleRepository interface communicates with the database
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
}
