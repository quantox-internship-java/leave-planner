package com.quantox.leaveplanner.repository;

import com.quantox.leaveplanner.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * UserRepository interface communicates with the database
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameAndStatus(String username, boolean status);
}
