package com.quantox.leaveplanner.repository;

import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.model.enumerations.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

/**
 * RequestRepository interface communicates with the database
 */
public interface RequestRepository extends JpaRepository<Request, Long> {

    @Query(value = "select employee, type, sum(total) as totalDaysOff from (select employee, type, count(type) * case when (datediff(end_date, start_date)) = 0 then 1 else (datediff(end_date, start_date))+1 end as total from request group by employee, type, end_date, start_date) as requestsPerEmployee group by type, employee", nativeQuery = true)
    List<String> groupRequestsByTypeAndEmployee();

    List<Request> findRequestsByEmployeeAndStartDateBetween(Employee employee, LocalDate startDate, LocalDate endDate);

    List<Request> findRequestByStartDateGreaterThanAndEndDateLessThanAndRequestStatus(LocalDate start, LocalDate end, RequestStatus s);

    @Query(value = "select employee, sum(total) as totalDaysOff, 20-sum(total) as daysRemaining from(select employee, type, count(type), request_status, count(type) * case when (datediff(end_date, start_date)) = 0 then 1 else (datediff(end_date, start_date))+1 end as total from request where request_status = 'APPROVED' group by employee, type, end_date, start_date) as daysOffPerEmployee group by employee", nativeQuery = true)
    List<String> daysOffRemaining();
}
