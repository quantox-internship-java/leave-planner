package com.quantox.leaveplanner.repository;

import com.quantox.leaveplanner.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * EmployeeRepository interface communicates with the database
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findById(Long id);

    boolean existsEmployeeByEmbg(String embg);

    Optional<Employee> findByUserId(Long id);
}
