package com.quantox.leaveplanner.controller;

import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.model.User;
import com.quantox.leaveplanner.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The UserController is responsible for handling requests about the User entity
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * This method returns a list of all users from the database
     *
     * @return List of users
     */
    @GetMapping("/users")
    public List<User> getUsers() {
        return this.userService.findAll();
    }


    /**
     * A method that creates a User
     *
     * @param userDTO {@link UserDTO}
     * @return User object
     */
    @PostMapping("/users/create")
    public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) {
        return this.userService.create(userDTO)
                .map(user -> ResponseEntity.ok().body(user))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    /**
     * A method that updates a User
     *
     * @param userDTO {@link UserDTO}
     * @return User object
     */
    @PostMapping("/users/update/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @Valid @RequestBody UserDTO userDTO) {
        return this.userService.update(id, userDTO)
                .map(user -> ResponseEntity.ok().body(user))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    /**
     * This method returns User by its id
     *
     * @param id ID
     * @return User
     */
    @GetMapping("/users/{id}")
    public ResponseEntity<User> findById(@PathVariable Long id) {
        return this.userService.findById(id)
                .map(user -> ResponseEntity.ok().body(user))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
