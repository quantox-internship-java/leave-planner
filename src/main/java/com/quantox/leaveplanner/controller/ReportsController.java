package com.quantox.leaveplanner.controller;

import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.report.RequestsExcelExporter;
import com.quantox.leaveplanner.service.RequestService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * The ReportsController is responsible for generating excel report
 * with approved requests from the current month
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class ReportsController {

    private final RequestService requestService;

    public ReportsController(RequestService requestService) {
        this.requestService = requestService;
    }

    /**
     * This method generates monthly report
     *
     * @param month    Month
     * @param response Response
     * @throws IOException
     */
    @GetMapping("/monthly_reports")
    public void findApprovedRequestsInCurrentMonth(@RequestParam Integer month,
                                                   HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=report_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<Request> requests = this.requestService.findAllApprovedRequestsInCurrentMonth(month);
        RequestsExcelExporter excelExporter = new RequestsExcelExporter(requests);
        excelExporter.export(response);
    }
}
