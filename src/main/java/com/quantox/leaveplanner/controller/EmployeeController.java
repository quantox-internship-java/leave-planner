package com.quantox.leaveplanner.controller;

import com.quantox.leaveplanner.dto.EmployeeDTO;
import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.service.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The EmployeeController is responsible for handling requests about the Employee entity
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * A method that creates an Employee
     *
     * @param employeeDTO {@link EmployeeDTO}
     * @return Employee object
     */
    @PostMapping("/employee/create")
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody EmployeeDTO employeeDTO) {
        return this.employeeService.create(employeeDTO)
                .map(employee -> ResponseEntity.ok().body(employee))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    /**
     * A method that updates an Employee
     *
     * @param employeeDTO {@link EmployeeDTO}
     * @return Employee object
     */
    @PostMapping("/employee/update/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable Long id, @Valid @RequestBody EmployeeDTO employeeDTO) {
        return this.employeeService.update(id, employeeDTO)
                .map(employee -> ResponseEntity.ok().body(employee))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    /**
     * This method returns a list of all employees from the database
     * @return a list of employees
     */
    @GetMapping("/employees")
    public List<Employee> getEmployees(){
        return this.employeeService.findAll();
    }

    /**
     * This method returns Employee by its id
     * @param id ID
     * @return Employee
     */
    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> findById(@PathVariable Long id) {
        return this.employeeService.findById(id)
                .map(employee -> ResponseEntity.ok().body(employee))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
