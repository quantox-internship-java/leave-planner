package com.quantox.leaveplanner.controller;

import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * This Rest Controller processes login information from the User
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    /**
     * This method maps "/login" page
     *
     * @param principal principal
     * @return userDTO {@link UserDTO}
     */
    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(Principal principal) {
        return ResponseEntity.ok(this.userService.findByUsername(principal.getName()));
    }
}
