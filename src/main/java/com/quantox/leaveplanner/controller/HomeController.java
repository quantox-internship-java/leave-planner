package com.quantox.leaveplanner.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The HomeController is responsible for handling requests about the Home page
 */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class HomeController {

    /**
     * A method that returns Home page and logged-in user's username
     *
     * @param authentication {@link Authentication}
     * @return logged-in user's username
     */
    @GetMapping("/home")
    public String getHomePage(Authentication authentication){
        return authentication.getName();
    }
}
