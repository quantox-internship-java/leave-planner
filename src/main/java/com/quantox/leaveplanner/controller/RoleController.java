package com.quantox.leaveplanner.controller;

import com.quantox.leaveplanner.model.Role;
import com.quantox.leaveplanner.service.RoleService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * The RoleController handles requests about the Role Entity
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    /**
     * A method that returns all the roles
     *
     * @return list of roles
     */
    @GetMapping("/roles")
    public List<Role> getRoles(){
        return this.roleService.findAll();
    }
}
