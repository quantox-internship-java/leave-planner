package com.quantox.leaveplanner.controller;

import com.quantox.leaveplanner.dto.ManagerReplyDTO;
import com.quantox.leaveplanner.dto.RequestDTO;
import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * The RequestController is responsible for handling requests for the Request entity
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class RequestController {

    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    /**
     * This method returns a list of employees' requested requests
     * that logged in project manager can manage
     *
     * @param principal Principal
     * @return List of Requests
     */
    @GetMapping("/requests")
    public List<Request> findAll(Principal principal) {
        return this.requestService.findAll(principal.getName());
    }

    /**
     * The createRequest method is responsible for creating a new Request
     *
     * @param requestDto {@link RequestDTO}
     * @return Request
     */
    @PostMapping("/request/create")
    public ResponseEntity<Request> createRequest(@Valid @RequestBody RequestDTO requestDto) {
        return this.requestService.create(requestDto)
                .map(request -> ResponseEntity.ok().body(request))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    /**
     * This method returns an existing Request by its id
     *
     * @param id ID
     * @return Request
     */
    @GetMapping("/request/{id}")
    public ResponseEntity<Request> findById(@PathVariable Long id) {
        return this.requestService.findById(id)
                .map(request -> ResponseEntity.ok().body(request))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * This method groups the Employee's requests by type
     *
     * @return list of strings
     */
    @GetMapping("/request/groupByType")
    public List<String> groupRequestsByType() {
        return this.requestService.groupRequestsByTypeAndEmployee();
    }

    /**
     * The managerReply method is responsible for responding to the Employee request
     *
     * @param managerReplyDTO {@link ManagerReplyDTO}
     * @return Request
     */
    @PostMapping("/request/manager_reply")
    public ResponseEntity<Request> managerReply(@Valid @RequestBody ManagerReplyDTO managerReplyDTO, Principal principal) {
        return this.requestService.managerReply(managerReplyDTO, principal.getName())
                .map(request -> ResponseEntity.ok().body(request))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    /**
     * Method that lists all  requests by given employee
     *
     * @param id ID
     * @return List of requested requests
     */
    @GetMapping("/request/listAllRequests/{id}")
    public List<Request> listAllRequests(@PathVariable Long id) {
        return this.requestService.listAllRequests(id);
    }

    /**
     * This method returns total days off and remaining days off
     * for each employee
     *
     * @return List of String
     */
    @GetMapping("/request/days_off_remaining")
    public List<String> daysOffRemaining() {
        return this.requestService.daysOffRemaining();
    }
}
