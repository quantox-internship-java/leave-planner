package com.quantox.leaveplanner.config;

import com.quantox.leaveplanner.model.enumerations.Role;
import com.quantox.leaveplanner.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * This class extends the WebSecurityConfigurerAdapter and implements the configure method
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;

    public WebSecurityConfig(UserService userService) {
        this.userService = userService;
    }

    /**
     * This method uses the AuthenticationManagerBuilder to authenticate the user
     *
     * @param auth {@link AuthenticationManagerBuilder}
     * @throws Exception Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(getPasswordEncoder());
    }

    /**
     * This method uses the HttpSecurity to authorize requests
     *
     * @param http {@link HttpSecurity}
     * @throws Exception Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/login",
                        "/home",
                        "/request/create",
                        "/request/listAllRequests")
                .authenticated()
                .antMatchers("/users",
                        "/users/create",
                        "/users/update")
                .hasRole(Role.ADMIN.name())
                .antMatchers("/employees",
                        "/employee/create",
                        "/employee/update",
                        "/requests",
                        "/request/manager_reply",
                        "/roles")
                .hasAnyRole(Role.ADMIN.name(),
                        Role.PROJECT_MANAGER.name(),
                        Role.OFFICE_ASSISTANT.name())
                .antMatchers("/monthly_reports")
                .hasRole(Role.OFFICE_ASSISTANT.name())
                .and()
                .logout()
                .logoutUrl("/logout")
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/login")
                .and()
                .httpBasic();
    }

    /**
     * This method uses PasswordEncoder to encrypt the password
     *
     * @return BCryptPasswordEncoder
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
