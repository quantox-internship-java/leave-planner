package com.quantox.leaveplanner.mapper;

import com.quantox.leaveplanner.dto.EmployeeDTO;
import com.quantox.leaveplanner.model.Employee;
import org.mapstruct.Mapper;

/**
 * This interface uses MapStruct to map Employee from EmployeeDTO
 */
@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    Employee sourceToDestination(EmployeeDTO employeeDTO);
}
