package com.quantox.leaveplanner.mapper;

import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.model.User;
import org.mapstruct.Mapper;

/**
 * This interface uses MapStruct to map User from UserDTO
 */
@Mapper(componentModel = "spring")
public interface UserMapper {
    User sourceToDestination(UserDTO userDTO);

    UserDTO destinationToSource(User user);
}
