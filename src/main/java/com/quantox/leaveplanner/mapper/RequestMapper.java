package com.quantox.leaveplanner.mapper;

import com.quantox.leaveplanner.dto.RequestDTO;
import com.quantox.leaveplanner.model.Request;
import org.mapstruct.Mapper;

/**
 * This interface uses MapStruct to map RequestDTO to Request
 */
@Mapper(componentModel = "spring")
public interface RequestMapper {
    Request sourceToDestination(RequestDTO requestDTO);
}
