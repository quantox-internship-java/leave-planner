package com.quantox.leaveplanner.service;

import com.quantox.leaveplanner.dto.ManagerReplyDTO;
import com.quantox.leaveplanner.dto.RequestDTO;
import com.quantox.leaveplanner.model.Request;

import java.util.List;
import java.util.Optional;

/**
 * An interface that defines methods about the Employee entity
 */
public interface RequestService {

    /**
     * This method returns all the employees' requests that the project manager manages
     *
     * @param principal Principal
     * @return List of Requests
     */
    List<Request> findAll(String principal);

    /**
     * This method is invoked when creating a new Request
     *
     * @param requestDto {@link RequestDTO}
     * @return Optional of Request
     */
    Optional<Request> create(RequestDTO requestDto);

    /**
     * A method that searches Request by its id and returns Optional of Request
     *
     * @param id ID
     * @return Optional of Request
     */
    Optional<Request> findById(Long id);

    /**
     * This method groups requests by type
     *
     * @return list of strings
     */
    List<String> groupRequestsByTypeAndEmployee();

    /**
     * A method that will respond to a particular request
     *
     * @param managerReplyDTO {@link ManagerReplyDTO}
     * @param username        Username
     * @return Optional of Request
     */
    Optional<Request> managerReply(ManagerReplyDTO managerReplyDTO, String username);

    /**
     * A method that will list all the requests by specific User
     *
     * @param userId ID
     * @return List of Requests
     */
    List<Request> listAllRequests(Long userId);

    /**
     * Returns all approved requests from the current month
     *
     * @return list of Requests
     */
    List<Request> findAllApprovedRequestsInCurrentMonth(Integer month);

    /**
     * Returns total days off and remaining days off for each employee
     *
     * @return List of String
     */
    List<String> daysOffRemaining();
}
