package com.quantox.leaveplanner.service;

import com.quantox.leaveplanner.dto.EmployeeDTO;
import com.quantox.leaveplanner.model.Employee;

import java.util.List;
import java.util.Optional;

/**
 * An interface that defines methods about the Employee entity
 */
public interface EmployeeService {

    /**
     * This method creates an Employee
     *
     * @param employeeDTO {@link EmployeeDTO}
     * @return Optional of Employee object
     */
    Optional<Employee> create(EmployeeDTO employeeDTO);

    /**
     * This method updates an Employee
     *
     * @param id          ID
     * @param employeeDTO {@link EmployeeDTO}
     * @return Optional of Employee object
     */
    Optional<Employee> update(Long id, EmployeeDTO employeeDTO);

    /**
     * A method that returns all the employees from the database
     *
     * @return a list of employees
     */
    List<Employee> findAll();

    /**
     * A method that searches Employee by its id and returns Optional of Employee
     *
     * @param id ID
     * @return Optional of Employee
     */
    Optional<Employee> findById(Long id);

    /**
     * Method that returns Employee by it's User ID
     *
     * @param id ID
     * @return Employee
     */
    Optional<Employee> findByUserId(Long id);
}
