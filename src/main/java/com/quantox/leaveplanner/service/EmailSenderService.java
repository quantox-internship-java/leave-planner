package com.quantox.leaveplanner.service;

import com.quantox.leaveplanner.model.Request;

public interface EmailSenderService {

    /**
     * This method sends an email when a new request has been created
     *
     * @param toEmail Mail of the Recipient
     * @param request {@link Request}
     */
    void sendEmail(String toEmail, Request request);
}
