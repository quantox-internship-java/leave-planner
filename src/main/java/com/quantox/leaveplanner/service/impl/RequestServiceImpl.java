package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.dto.ManagerReplyDTO;
import com.quantox.leaveplanner.dto.RequestDTO;
import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.mapper.RequestMapper;
import com.quantox.leaveplanner.mapper.UserMapper;
import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.model.User;
import com.quantox.leaveplanner.model.enumerations.RequestStatus;
import com.quantox.leaveplanner.repository.RequestRepository;
import com.quantox.leaveplanner.service.EmailSenderService;
import com.quantox.leaveplanner.service.EmployeeService;
import com.quantox.leaveplanner.service.RequestService;
import com.quantox.leaveplanner.service.UserService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * RequestServiceImpl class is responsible for handling requests for the Request entity
 * from controllers and communicating with RequestRepository interface
 * <p>
 * It implements the methods from RequestService interface
 */
@Service
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final RequestMapper requestMapper;
    private final EmployeeService employeeService;
    private final UserService userService;
    private final UserMapper userMapper;
    private final EmailSenderService emailSenderService;

    public RequestServiceImpl(RequestRepository requestRepository, RequestMapper requestMapper, EmployeeService employeeService, UserService userService, EmailSenderService emailSenderService, UserMapper userMapper) {
        this.requestRepository = requestRepository;
        this.requestMapper = requestMapper;
        this.employeeService = employeeService;
        this.userService = userService;
        this.emailSenderService = emailSenderService;
        this.userMapper = userMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Request> findAll(String principal) {
        UserDTO user = this.userService.findByUsername(principal);
        return this.requestRepository.findAll().stream().filter(r -> {
            boolean matching = false;
            for (Employee employee : user.getEmployees()) {
                matching = employee.getId().equals(r.getEmployee().getId());
                if (matching) {
                    break;
                }
            }
            return matching;
        }).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Request> create(RequestDTO requestDto) {
        Optional<User> user = this.userService.findById(requestDto.getUserId());
        if (user.isEmpty()) {
            return Optional.empty();
        }
        Optional<Employee> employee = this.employeeService.findByUserId(user.get().getId());
        if (employee.isEmpty()) {
            return Optional.empty();
        }
        Request request = requestMapper.sourceToDestination(requestDto);
        request.setRequestStatus(RequestStatus.PENDING);
        if (employee.isPresent()) {
            request.setEmployee(employee.get());
            this.requestRepository.save(request);

            for (User manager : employee.get().getUsers()) {
                emailSenderService.sendEmail(manager.getEmail(), request);
            }
        }
        return Optional.of(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Request> findById(Long id) {
        return this.requestRepository.findById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> groupRequestsByTypeAndEmployee() {
        return this.requestRepository.groupRequestsByTypeAndEmployee();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Request> managerReply(ManagerReplyDTO managerReplyDTO, String username) {
        UserDTO user = this.userService.findByUsername(username);
        if (user == null) {
            return Optional.empty();
        }

        Optional<Request> optionalRequest = requestRepository.findById(managerReplyDTO.getId());
        if (optionalRequest.isEmpty()) {
            return Optional.empty();
        }

        Request request = optionalRequest.get();
        request.setRequestManager(this.userMapper.sourceToDestination(user));
        request.setRequestStatus(managerReplyDTO.getRequestStatus());
        request.setRequestDescription(managerReplyDTO.getRequestDescription());
        request.setRequestDate(LocalDate.now());

        return Optional.of(this.requestRepository.save(request));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Request> listAllRequests(Long userId) {
        Optional<User> user = userService.findById(userId);
        if (user.isEmpty()) {
            return new ArrayList<>();
        }

        Optional<Employee> employee = this.employeeService.findByUserId(user.get().getId());
        if (employee.isEmpty()) {
            return new ArrayList<>();
        }

        LocalDate startDate = LocalDate.of(LocalDate.now().getYear(), Month.JANUARY, 1);
        LocalDate endDate = LocalDate.of(LocalDate.now().getYear(), Month.DECEMBER, 31);

        return requestRepository.findRequestsByEmployeeAndStartDateBetween(employee.get(), startDate, endDate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Request> findAllApprovedRequestsInCurrentMonth(Integer month) {
        LocalDate selectedMonth = LocalDate.now().withMonth(month);
        LocalDate startDate = selectedMonth.withDayOfMonth(1);
        LocalDate endDate = selectedMonth.withDayOfMonth(selectedMonth.lengthOfMonth());

        RequestStatus status = RequestStatus.APPROVED;
        return this.requestRepository.findRequestByStartDateGreaterThanAndEndDateLessThanAndRequestStatus(startDate, endDate, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> daysOffRemaining() {
        return this.requestRepository.daysOffRemaining();
    }
}
