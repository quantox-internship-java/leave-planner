package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.mapper.UserMapper;
import com.quantox.leaveplanner.model.User;
import com.quantox.leaveplanner.model.exceptions.UserNotFoundException;
import com.quantox.leaveplanner.model.exceptions.UsernameAlreadyExistsException;
import com.quantox.leaveplanner.repository.UserRepository;
import com.quantox.leaveplanner.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * This Service Bean implements the UserService interface
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    /**
     * This method checks if the User exists in the database - saves him if he doesn't
     *
     * @param username Username
     * @return User {@link User}
     * @throws UsernameNotFoundException Username Not Found Exception
     */
    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<User> userOptional = userRepository.findByUsernameAndStatus(username, true);
        User user = userOptional.orElseThrow(() -> new UsernameNotFoundException(username + " not found!"));

        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getUsername())
                .password(user.getPassword()).authorities("ROLE_" + user.getRole().getName()).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findById(Long id) {
        return this.userRepository.findById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDTO findByUsername(String username) {
        Optional<User> user = this.userRepository.findByUsername(username);
        return this.userMapper.destinationToSource(user.get());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> create(UserDTO userDTO) {
        User user = userMapper.sourceToDestination(userDTO);
        user.setPassword(encodePassword(user.getPassword()));
        if (userRepository.findByUsername(userDTO.getUsername()).isPresent()) {
            throw new UsernameAlreadyExistsException();
        } else {
            this.userRepository.save(user);
        }
        return Optional.of(user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> update(Long id, UserDTO userDTO) {
        User user = userMapper.sourceToDestination(userDTO);
        User databaseUser = this.userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
        user.setId(databaseUser.getId());
        if (user.getPassword() == null) {
            user.setPassword(databaseUser.getPassword());
        } else {
            user.setPassword(encodePassword(user.getPassword()));
        }
        this.userRepository.save(user);
        return Optional.of(user);
    }

    /**
     * This method encodes User's password
     *
     * @param password Password
     * @return Encrypted password
     */
    private String encodePassword(String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(password);
    }
}

