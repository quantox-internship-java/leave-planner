package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.dto.EmployeeDTO;
import com.quantox.leaveplanner.mapper.EmployeeMapper;
import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.exceptions.EmployeeNotFoundException;
import com.quantox.leaveplanner.model.exceptions.EmployeeWithEmbgAlreadyExists;
import com.quantox.leaveplanner.repository.EmployeeRepository;
import com.quantox.leaveplanner.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * EmployeeServiceImpl class is responsible for handling requests about Employee entity
 * from controllers and communicating with EmployeeRepository interface
 * <p>
 * It implements the methods from EmployeeService interface
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Employee> create(EmployeeDTO employeeDTO) {
        Employee employee = employeeMapper.sourceToDestination(employeeDTO);
        if (this.employeeRepository.existsEmployeeByEmbg(employeeDTO.getEmbg())) {
            throw new EmployeeWithEmbgAlreadyExists(employeeDTO.getEmbg());
        } else {
            this.employeeRepository.save(employee);
        }
        this.employeeRepository.save(employee);
        return Optional.of(employee);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Employee> update(Long id, EmployeeDTO employeeDTO) {
        Employee employee = employeeMapper.sourceToDestination(employeeDTO);
        Employee databaseEmployee = this.employeeRepository.findById(id).orElseThrow(() -> new EmployeeNotFoundException(id));
        employee.setId(databaseEmployee.getId());
        this.employeeRepository.save(employee);
        return Optional.of(employee);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Employee> findAll() {
        return this.employeeRepository.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Employee> findById(Long id) {
        return this.employeeRepository.findById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Employee> findByUserId(Long id) {
        return this.employeeRepository.findByUserId(id);
    }
}
