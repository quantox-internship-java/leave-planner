package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.model.Role;
import com.quantox.leaveplanner.repository.RoleRepository;
import com.quantox.leaveplanner.service.RoleService;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * RoleServiceImpl class is responsible for handling requests about the Role Entity
 * from the RoleController and communicating with RoleRepository interface
 *
 * It implements the methods from the RoleService interface
 */
@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    /**
     * A method that returns all the roles from the database
     *
     * @return list of roles
     */
    @Override
    public List<Role> findAll() {
        return this.roleRepository.findAll();
    }
}
