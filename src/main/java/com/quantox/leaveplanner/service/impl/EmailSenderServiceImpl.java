package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.service.EmailSenderService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {

    private final JavaMailSender mailSender;

    public EmailSenderServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * {@inheritDoc}
     */
    public void sendEmail(String toEmail, Request request) {
        StringBuilder stringBuilder = new StringBuilder();
        String requestInfo = stringBuilder
                .append("Hi ").append(request.getEmployee().getUser().getUsername()).append("\n \n")

                .append(request.getEmployee().getName() + " " + request.getEmployee().getSurname())
                .append(" requested days off for period from ").append(request.getStartDate())
                .append(" to ").append(request.getEndDate()).append(". \n")

                .append("Please make the approval/decline here http://localhost:4200/request/manager_reply/")
                .append(request.getId()).append("\n \n")
                .append("Kind regards,")
                .toString();

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(toEmail);
        message.setText(requestInfo);
        message.setSubject("Vacation Request from " + request.getEmployee().getName());

        mailSender.send(message);
    }
}
