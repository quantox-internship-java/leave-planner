package com.quantox.leaveplanner.service;

import com.quantox.leaveplanner.model.Role;
import java.util.List;

/**
 * RoleService interface defines methods for the Role Entity
 */
public interface RoleService {

    List<Role> findAll();
}
