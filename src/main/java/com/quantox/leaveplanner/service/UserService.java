package com.quantox.leaveplanner.service;

import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

/**
 * This interface extends the UserDetailService
 */
public interface UserService extends UserDetailsService {

    /**
     * This method returns all the Users from the database
     *
     * @return List of Users
     */
    List<User> findAll();

    /**
     * A method that creates a User
     *
     * @param userDTO {@link UserDTO}
     * @return Optional of User
     */
    Optional<User> create(UserDTO userDTO);

    /**
     * A method that updates a User
     *
     * @param id ID
     * @param userDTO userDTO
     * @return Optional of User
     */
    Optional<User> update(Long id, UserDTO userDTO);

    /**
     * A method that searches User by its ID - returns Optional of User
     *
     * @param id ID
     * @return Optional of User
     */
    Optional<User> findById(Long id);

    /**
     * This method returns User by username
     *
     * @param username Username
     * @return Optional of User
     */
    UserDTO findByUsername(String username);
}
