package com.quantox.leaveplanner.repository;

import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.model.User;
import com.quantox.leaveplanner.model.enumerations.Status;
import com.quantox.leaveplanner.model.enumerations.Type;
import com.quantox.leaveplanner.service.RequestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RequestRepositoryTest {

    public static final LocalDate REQUEST_START_DATE = LocalDate.now();
    public static final LocalDate REQUEST_END_DATE = LocalDate.now().plusDays(1);
    public static final String REQUEST_TYPE = "Vacation";
    public static final String REQUEST_TYPE_2 = "Day off";
    public static final String REQUEST_TYPE_3 = "Sick leave";
    public static final String REQUEST_DESCRIPTION = "Test description";
    public static final String EMPLOYEE_NAME = "Test";
    public static final String EMPLOYEE_SURNAME = "Surname";
    public static final String EMPLOYEE_EMAIL = "test@test.com";
    public static final String EMPLOYEE_COUNTRY = "Macedonia";
    public static final String EMPLOYEE_OFFICE = "Skopje";
    public static final String EMPLOYEE_EMBG = "1231231231";

    public static final String USER_USERNAME = "test";

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RequestService requestService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void init() {
        User user = createUser();

        Employee employee = getEmployee();
        employee.setUser(user);
        ArrayList<User> users = new ArrayList<>();
        users.add(user);
        employee.setUsers(users);

        this.employeeRepository.save(employee);

        Request request = createRequest();
        request.setEmployee(employee);

        requestRepository.save(request);
    }

    @Test
    public void findAll() {
        List<Request> requests = this.requestService.findAll(USER_USERNAME);
        assertEquals(1, requests.size());
    }

    @Test
    @DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
    public void testGroupRequestsByType() {
        Optional<Employee> employee = employeeRepository.findByUserId(1L);

        Request request = new Request();
        request.setStartDate(REQUEST_START_DATE);
        request.setEndDate(REQUEST_END_DATE);
        request.setType(REQUEST_TYPE);
        request.setDescription(REQUEST_DESCRIPTION);
        request.setEmployee(employee.get());

        Request request2 = new Request();
        request2.setStartDate(REQUEST_START_DATE);
        request2.setEndDate(REQUEST_END_DATE);
        request2.setType(REQUEST_TYPE_2);
        request2.setDescription(REQUEST_DESCRIPTION);
        request2.setEmployee(employee.get());

        Request request3 = new Request();
        request3.setStartDate(REQUEST_START_DATE);
        request3.setEndDate(REQUEST_END_DATE);
        request3.setType(REQUEST_TYPE_3);
        request3.setDescription(REQUEST_DESCRIPTION);
        request3.setEmployee(employee.get());

        requestRepository.save(request);
        requestRepository.save(request2);
        requestRepository.save(request3);

        String expected = "2,Day off,1";
        String expected2 = "2,Sick leave,1";
        String expected3 = "2,Vacation,2";

        List<String> requests = this.requestService.groupRequestsByTypeAndEmployee();
        assertEquals(expected, requests.get(0));
        assertEquals(expected2, requests.get(1));
        assertEquals(expected3, requests.get(2));
    }

    private Employee getEmployee() {
        Employee employee = new Employee();
        employee.setName(EMPLOYEE_NAME);
        employee.setSurname(EMPLOYEE_SURNAME);
        employee.setCountry(EMPLOYEE_COUNTRY);
        employee.setEmail(EMPLOYEE_EMAIL);
        employee.setStatus(Status.ACTIVE);
        employee.setType(Type.INTERN);
        employee.setOffice(EMPLOYEE_OFFICE);
        employee.setEmbg(EMPLOYEE_EMBG);
        return employee;
    }

    public User createUser() {
        User user = new User();
        user.setUsername(USER_USERNAME);

        return this.userRepository.save(user);
    }

    public Request createRequest() {
        Request request = new Request();
        request.setStartDate(REQUEST_START_DATE);
        request.setEndDate(REQUEST_END_DATE);
        request.setType(REQUEST_TYPE);
        request.setDescription(REQUEST_DESCRIPTION);

        return request;
    }
}
