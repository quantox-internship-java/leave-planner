package com.quantox.leaveplanner.repository;

import com.quantox.leaveplanner.model.Role;
import com.quantox.leaveplanner.service.RoleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleRepositoryTest {

    public static final String ROLE_NAME = "ROLE";
    public static final String ROLE_DESCRIPTION = "ROLE DESCRIPTION";

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleService roleService;

    @Before
    public void init(){
        if(roleRepository.count() == 0){
            Role role = getRole();
            roleRepository.save(role);
        }
    }

    @Test
    public void findAll(){
        List<Role> roles = this.roleService.findAll();
        assertEquals(1, roles.size());
    }

    private Role getRole(){
        Role role = new Role();
        role.setName(ROLE_NAME);
        role.setDescription(ROLE_DESCRIPTION);

        return role;
    }

}
