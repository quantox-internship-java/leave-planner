package com.quantox.leaveplanner.repository;

import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.enumerations.Status;
import com.quantox.leaveplanner.model.enumerations.Type;
import com.quantox.leaveplanner.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Before
    public void init(){
        if(employeeRepository.count() == 0){
            Employee employee = new Employee();
            employee.setName("Employee name");
            employee.setSurname("Employee surname");
            employee.setEmbg("1122334455");
            employee.setType(Type.INTERN);
            employee.setStatus(Status.ACTIVE);
            employee.setEmail("employee@employee.com");
            employee.setCountry("Macedonia");
            employee.setOffice("Skopje");

            employeeRepository.save(employee);
        }
    }

    @Test
    public void findAll(){
        List<Employee> employeeList = this.employeeService.findAll();
        assertEquals(1, employeeList.size());
    }
}
