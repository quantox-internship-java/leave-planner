package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.mapper.EmployeeMapper;
import com.quantox.leaveplanner.dto.EmployeeDTO;
import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.enumerations.Status;
import com.quantox.leaveplanner.model.enumerations.Type;
import com.quantox.leaveplanner.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class EmployeeServiceImplTest {

    public static final long EMPLOYEE_ID = 1L;
    public static final String EMPLOYEE_NAME = "Test";
    public static final String EMPLOYEE_SURNAME = "Surname";
    public static final String EMPLOYEE_EMAIL = "test@test.com";
    public static final String EMPLOYEE_COUNTRY = "Macedonia";
    public static final String EMPLOYEE_OFFICE = "Skopje";
    public static final String EMPLOYEE_EMBG = "1231231231";
    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private EmployeeMapper employeeMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreate() {
        EmployeeDTO employeeDTO = createEmployeeDTO();
        Employee employee = getEmployee();

        when(employeeMapper.sourceToDestination(employeeDTO)).thenReturn(employee);

        Optional<Employee> result = employeeService.create(employeeDTO);

        if (result.isPresent()) {
            Employee actual = result.get();
            assertEquals(EMPLOYEE_NAME, actual.getName());
            assertEquals(EMPLOYEE_SURNAME, actual.getSurname());
            assertEquals(EMPLOYEE_EMAIL, actual.getEmail());
            assertEquals(EMPLOYEE_OFFICE, actual.getOffice());
            assertEquals(EMPLOYEE_COUNTRY, actual.getCountry());
            assertEquals(EMPLOYEE_EMBG, actual.getEmbg());
            assertEquals(Type.INTERN, actual.getType());
            assertEquals(Status.ACTIVE, actual.getStatus());
        }
    }

    @Test
    void testUpdate() {

        EmployeeDTO employeeDTO = createEmployeeDTO();
        Employee employee = getEmployee();

        when(employeeMapper.sourceToDestination(employeeDTO)).thenReturn(employee);
        when(this.employeeRepository.findById(EMPLOYEE_ID)).thenReturn(Optional.of(employee));

        Optional<Employee> result = employeeService.update(EMPLOYEE_ID, employeeDTO);

        if (result.isPresent()) {
            Employee actual = result.get();
            assertEquals(EMPLOYEE_NAME, actual.getName());
            assertEquals(EMPLOYEE_SURNAME, actual.getSurname());
            assertEquals(EMPLOYEE_EMAIL, actual.getEmail());
            assertEquals(EMPLOYEE_OFFICE, actual.getOffice());
            assertEquals(EMPLOYEE_COUNTRY, actual.getCountry());
            assertEquals(EMPLOYEE_EMBG, actual.getEmbg());
            assertEquals(Type.INTERN, actual.getType());
            assertEquals(Status.ACTIVE, actual.getStatus());
        }
    }

    private Employee getEmployee() {
        Employee employee = new Employee();
        employee.setName(EMPLOYEE_NAME);
        employee.setSurname(EMPLOYEE_SURNAME);
        employee.setCountry(EMPLOYEE_COUNTRY);
        employee.setEmail(EMPLOYEE_EMAIL);
        employee.setStatus(Status.ACTIVE);
        employee.setType(Type.INTERN);
        employee.setOffice(EMPLOYEE_OFFICE);
        employee.setEmbg(EMPLOYEE_EMBG);
        return employee;
    }

    private EmployeeDTO createEmployeeDTO() {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setName(EMPLOYEE_NAME);
        employeeDTO.setSurname(EMPLOYEE_SURNAME);
        employeeDTO.setCountry(EMPLOYEE_COUNTRY);
        employeeDTO.setEmail(EMPLOYEE_EMAIL);
        employeeDTO.setStatus(Status.ACTIVE);
        employeeDTO.setType(Type.INTERN);
        employeeDTO.setOffice(EMPLOYEE_OFFICE);
        employeeDTO.setEmbg(EMPLOYEE_EMBG);
        return employeeDTO;
    }
}
