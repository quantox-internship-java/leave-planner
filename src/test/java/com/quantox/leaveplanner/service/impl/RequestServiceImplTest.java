package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.dto.EmployeeDTO;
import com.quantox.leaveplanner.dto.ManagerReplyDTO;
import com.quantox.leaveplanner.dto.RequestDTO;
import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.mapper.RequestMapper;
import com.quantox.leaveplanner.mapper.UserMapper;
import com.quantox.leaveplanner.model.Employee;
import com.quantox.leaveplanner.model.Request;
import com.quantox.leaveplanner.model.User;
import com.quantox.leaveplanner.model.enumerations.RequestStatus;
import com.quantox.leaveplanner.model.enumerations.Role;
import com.quantox.leaveplanner.model.enumerations.Status;
import com.quantox.leaveplanner.model.enumerations.Type;
import com.quantox.leaveplanner.repository.RequestRepository;
import com.quantox.leaveplanner.service.EmployeeService;
import com.quantox.leaveplanner.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RequestServiceImplTest {

    public static final long REQUEST_ID = 1L;
    public static final LocalDate REQUEST_START_DATE = LocalDate.now();
    public static final LocalDate REQUEST_END_DATE = LocalDate.now().plusDays(1);
    public static final String REQUEST_TYPE = "Vacation";
    public static final String REQUEST_DESCRIPTION = "Test description";
    public static final Long REQUEST_USER_ID = 1L;

    public static final Long EMPLOYEE_ID = 1L;
    public static final String EMPLOYEE_NAME = "Test";
    public static final String EMPLOYEE_SURNAME = "Surname";
    public static final String EMPLOYEE_EMAIL = "test@test.com";
    public static final String EMPLOYEE_COUNTRY = "Macedonia";
    public static final String EMPLOYEE_OFFICE = "Skopje";
    public static final String EMPLOYEE_EMBG = "1231231231";

    private static final Long USER_ID = 1L;
    private static final String USER_USERNAME = "USERNAME_USER";
    private static final String USER_EMAIL = "test@quantox.com";
    private static final String USER_PASSWORD = "USER_PASSWORD";
    private static final Boolean USER_STATUS = Boolean.TRUE;
    private static final Role USER_ROLE = Role.ADMIN;
    private static final LocalDate START_DATE = LocalDate.of(LocalDate.now().getYear(), Month.JANUARY, 1);
    private static final LocalDate END_DATE = LocalDate.of(LocalDate.now().getYear(), Month.DECEMBER, 31);

    private static final long MANAGER_ID = 1L;
    private static final RequestStatus REQUEST_STATUS = RequestStatus.APPROVED;
    private static final String MANAGER_DESCRIPTION = "Enjoy your vacation";
    private static final LocalDate MANAGER_REPLY_DATE = LocalDate.now();
    private static final String USERNAME = "username";

    @InjectMocks
    private RequestServiceImpl requestService;

    @Mock
    private RequestMapper requestMapper;

    @Mock
    private UserService userService;

    @Mock
    private EmployeeService employeeService;

    @Mock
    private RequestRepository requestRepository;

    @Mock
    private UserMapper userMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreate() {
        RequestDTO requestDto = createRequestDto();
        Request request = getRequest();

        when(requestMapper.sourceToDestination(requestDto)).thenReturn(request);
        when(userService.findById(any())).thenReturn(Optional.of(new User()));
        when(employeeService.findByUserId(any())).thenReturn(Optional.of(new Employee()));

        Optional<Request> result = requestService.create(requestDto);

        if (result.isPresent()) {
            Request actual = result.get();
            assertEquals(REQUEST_START_DATE, actual.getStartDate());
            assertEquals(REQUEST_END_DATE, actual.getEndDate());
            assertEquals(REQUEST_TYPE, actual.getType());
            assertEquals(REQUEST_DESCRIPTION, actual.getDescription());
        }
    }

    @Test
    void testFindById() {
        Request request = getRequest();

        when(this.requestRepository.findById(REQUEST_ID)).thenReturn(Optional.of(request));

        Optional<Request> result = requestService.findById(REQUEST_ID);

        if (result.isPresent()) {
            Request actual = result.get();
            assertEquals(REQUEST_START_DATE, actual.getStartDate());
            assertEquals(REQUEST_END_DATE, actual.getEndDate());
            assertEquals(REQUEST_TYPE, actual.getType());
            assertEquals(REQUEST_DESCRIPTION, actual.getDescription());
        }
    }

    @Test
    void testManagerReply() {
        ManagerReplyDTO managerReplyDTO = createManagerDto();
        Request request = getRequest();

        request.setId(managerReplyDTO.getId());
        request.setRequestStatus(managerReplyDTO.getRequestStatus());
        request.setRequestDescription(managerReplyDTO.getRequestDescription());
        request.setRequestDate(managerReplyDTO.getRequestDate());

        User user = new User();
        user.setUsername("Risto123");

        when(userService.findByUsername(any())).thenReturn(new UserDTO());
        when(requestRepository.findById(any())).thenReturn(Optional.of(request));
        when(requestRepository.save(any())).thenReturn(request);

        Optional<Request> result = requestService.managerReply(managerReplyDTO, USERNAME);

        if (result.isPresent()) {
            Request actual = result.get();
            assertEquals(MANAGER_ID, actual.getId());
            assertEquals(REQUEST_STATUS, actual.getRequestStatus());
            assertEquals(MANAGER_DESCRIPTION, actual.getRequestDescription());
            assertEquals(MANAGER_REPLY_DATE, actual.getRequestDate());
        }
    }

    @Test
    void testManagerReplyWhenUserIsEmpty() {
        ManagerReplyDTO managerReplyDTO = createManagerDto();
        when(userService.findByUsername(any())).thenReturn(new UserDTO());

        Optional<Request> result = requestService.managerReply(managerReplyDTO, USERNAME);

        assertEquals(Optional.empty(), result);
    }

    @Test
    void testManagerReplyWhenRequestIsEmpty() {
        ManagerReplyDTO managerReplyDTO = createManagerDto();
        when(requestService.findById(any())).thenReturn(Optional.empty());

        Optional<Request> result = requestService.managerReply(managerReplyDTO, USERNAME);

        assertEquals(Optional.empty(), result);
    }

    @Test
    void testListAllUsers() {
        User user = getUser();
        Employee employee = getEmployee();
        Request request = getRequest();

        when(userService.findById(any())).thenReturn(Optional.of(user));
        when(employeeService.findByUserId(any())).thenReturn(Optional.of(employee));
        when(requestRepository.findRequestsByEmployeeAndStartDateBetween(employee, START_DATE, END_DATE))
                .thenReturn(List.of(request));

        assertEquals(1, List.of(request).size());
    }

    private Request getRequest() {
        Request request = new Request();
        request.setStartDate(REQUEST_START_DATE);
        request.setEndDate(REQUEST_END_DATE);
        request.setType(REQUEST_TYPE);
        request.setDescription(REQUEST_DESCRIPTION);
        request.setEmployee(getEmployee());
        return request;
    }

    private Employee getEmployee() {
        Employee employee = new Employee();
        employee.setId(EMPLOYEE_ID);
        employee.setName(EMPLOYEE_NAME);
        employee.setSurname(EMPLOYEE_SURNAME);
        employee.setEmail(EMPLOYEE_EMAIL);
        employee.setCountry(EMPLOYEE_COUNTRY);
        employee.setOffice(EMPLOYEE_OFFICE);
        employee.setEmbg(EMPLOYEE_EMBG);

        return employee;
    }

    private RequestDTO createRequestDto() {
        RequestDTO requestDto = new RequestDTO();
        requestDto.setStartDate(REQUEST_START_DATE);
        requestDto.setEndDate(REQUEST_END_DATE);
        requestDto.setType(REQUEST_TYPE);
        requestDto.setDescription(REQUEST_DESCRIPTION);
        requestDto.setUserId(REQUEST_USER_ID);
        return requestDto;
    }

    private EmployeeDTO createEmployeeDto() {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setName(EMPLOYEE_NAME);
        employeeDTO.setSurname(EMPLOYEE_SURNAME);
        employeeDTO.setCountry(EMPLOYEE_COUNTRY);
        employeeDTO.setEmail(EMPLOYEE_EMAIL);
        employeeDTO.setStatus(Status.ACTIVE);
        employeeDTO.setType(Type.INTERN);
        employeeDTO.setOffice(EMPLOYEE_OFFICE);
        employeeDTO.setEmbg(EMPLOYEE_EMBG);
        return employeeDTO;
    }

    private ManagerReplyDTO createManagerDto() {
        ManagerReplyDTO managerReplyDTO = new ManagerReplyDTO();
        managerReplyDTO.setId(MANAGER_ID);
        managerReplyDTO.setRequestStatus(REQUEST_STATUS);
        managerReplyDTO.setRequestDescription(MANAGER_DESCRIPTION);
        managerReplyDTO.setRequestDate(MANAGER_REPLY_DATE);
        return managerReplyDTO;
    }

    private User getUser() {
        User user = new User();
        user.setId(USER_ID);
        user.setUsername(USER_USERNAME);
        user.setEmail(USER_EMAIL);
        user.setPassword(USER_PASSWORD);
        user.setStatus(USER_STATUS);

        return user;
    }
}
