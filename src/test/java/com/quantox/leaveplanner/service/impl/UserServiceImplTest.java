package com.quantox.leaveplanner.service.impl;

import com.quantox.leaveplanner.dto.UserDTO;
import com.quantox.leaveplanner.mapper.UserMapper;
import com.quantox.leaveplanner.model.User;
import com.quantox.leaveplanner.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    public static final long USER_ID = 1L;
    private static final String USER_USERNAME = "Test";
    private static final String USER_EMAIL = "test@test.com";
    private static final String USER_PASSWORD = "testPassword";
    private static final Boolean USER_STATUS = true;
    private static final String USERNAME = "Test 2";

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Before
    public void init() {
        User user = new User();
        user.setUsername(USER_USERNAME);
        user.setEmail(USER_EMAIL);
        user.setPassword(USER_PASSWORD);
        user.setStatus(USER_STATUS);
        userRepository.save(user);
    }

    @Test
    public void findAll() {
        List<User> userList = this.userService.findAll();
        assertEquals(1, userList.size());
    }

    @Test
    public void testCreate() {

        UserDTO userDTO = createUserDTO();
        User user = getUser();

        when(userMapper.sourceToDestination(userDTO)).thenReturn(user);

        Optional<User> result = userService.create(userDTO);

        if (result.isPresent()) {
            User actual = result.get();
            assertEquals(USERNAME, actual.getUsername());
            assertEquals(USER_EMAIL, actual.getEmail());
            assertEquals(USER_STATUS, actual.getStatus());
        }
    }

    @Test
    public void testUpdate() {

        UserDTO userDTO = createUserDTO();
        User user = getUser();

        when(userMapper.sourceToDestination(userDTO)).thenReturn(user);

        UserRepository mock = org.mockito.Mockito.mock(UserRepository.class);
        when(mock.getById(USER_ID)).thenReturn(user);

        Optional<User> result = userService.update(USER_ID, userDTO);

        if (result.isPresent()) {
            User actual = result.get();
            assertEquals(USER_ID, actual.getId());
            assertEquals(USERNAME, actual.getUsername());
            assertEquals(USER_EMAIL, actual.getEmail());
            assertEquals(USER_STATUS, actual.getStatus());
        }
    }

    private User getUser() {
        User user = new User();
        user.setUsername(USERNAME);
        user.setEmail(USER_EMAIL);
        user.setPassword(USER_PASSWORD);
        user.setStatus(USER_STATUS);
        return user;
    }

    private UserDTO createUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(USERNAME);
        userDTO.setEmail(USER_EMAIL);
        userDTO.setPassword(USER_PASSWORD);
        userDTO.setStatus(USER_STATUS);
        return userDTO;
    }
}
