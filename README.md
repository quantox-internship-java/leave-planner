# Leave Planner

## Table of contents
* [Main objectives of the project](#main-objectives-of-the-project)
* [Technologies](#technologies)
* [Setup](#setup)

## Main objectives of the project

Leave Planner is an application for employees in a company to ask for a day off. As they request a day (or days) off, 
an email is sent automatically to their manager, who can approve or decline their request. The employee can select one 
from the several provided options to take a leave.

## Technologies

* **Spring** (version 2.6.5)
* **Angular** (version 13.3.3)
* **Database - MySQL** (version 8.0.28) *only for localhost*
* **ORM - Hibernate** (version 5.6.7.Final)
* **MapStruct** (version 1.4.2.Final)
* **WebServer embedded Tomcat** (version 9.0.60)
* **Database version control - Flyway** (version 8.0.0)


## Setup

```
$ mvn clean install
$ java -jar leave-planner-0.0.1-SNAPSHOT.war
```